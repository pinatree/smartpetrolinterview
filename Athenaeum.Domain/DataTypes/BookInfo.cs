﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Athenaeum.Domain.DataTypes
{
    public class BookInfo
    {
        public string ISBN { get; set; }
    }
}
